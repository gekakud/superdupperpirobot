﻿using RoboticCar.PC.Common.InterfacesBL;
using RoboticCar.PC.Common.Logging;

namespace RoboticCar.PC.BL
{
    public class ImageProcessor : IImageProcessor
    {
        #region IImageProcessor Members

        public void ProcessImage(byte[] image)
        {
            Logger.Trace("ImageProcessor.ProcessImage: Started");

            Logger.Trace("ImageProcessor.ProcessImage: Ended");
        }

        #endregion
    }
}
