﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoboticCar.PC.Common.InterfacesCallback;

namespace RoboticCar.PC.Common.InterfacesView
{
    public interface ICarControlView<TCallback> : IActionView<TCallback>
    {
        IConnectView<IConnectCallback> GetConnectionView { get; }
    }
}
