﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboticCar.PC.Common.Exceptions
{
    [Serializable]
    public class StartTaskException : Exception
    {
        public StartTaskException(string info, Exception innerException)
            : base(info, innerException)
        {
        }
    }
}
