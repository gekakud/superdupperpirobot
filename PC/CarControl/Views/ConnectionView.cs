﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoboticCar.PC.Common.InterfacesCallback;
using RoboticCar.PC.Common.InterfacesView;

namespace RoboticCar.PC.CarControlUI.Views
{
    public partial class ConnectionView : UserControl, IConnectView<IConnectCallback>
    {
        public ConnectionView()
        {
            InitializeComponent();
        }

        #region IView<IConnectCallback> Members

        public IConnectCallback Callback { get; set; }

        public ICarControlView<ICarControlCallback> CarControlView { get; set; }

        #endregion

        private void connectBtn_Click(object sender, EventArgs e)
        {
            if (Callback != null)
            {
                int port = 0;
                int.TryParse(portTB.Text, out port);
                if (port > 0)
                {
                    Callback.ConnectToServer(ipTB.Text, int.Parse(portTB.Text));
                }
                else
                {
                    MessageBox.Show("Wrong port!");
                }
            }
                
        }
    }
}
