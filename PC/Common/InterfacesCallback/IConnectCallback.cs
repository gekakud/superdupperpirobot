﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboticCar.PC.Common.InterfacesCallback
{
    public interface IConnectCallback
    {
        void ConnectToServer(string ip, int port);
        void Disconnect();
    }
}
