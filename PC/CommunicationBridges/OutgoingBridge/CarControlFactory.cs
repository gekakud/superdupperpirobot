﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboticCar.PC.CommunicationBriges.OutgoingBridge
{
    public interface ICarControl
    {
        bool Initialize();
        void Uninitialize();
        string IP { get; }
        int Port { get; }
        bool IsInitialized { get; }

        bool GoForward(int speed);
        bool GoBackward(int speed);
        bool TurnLeft(int angle, bool wheelFixed);
        bool TurnRight(int angle, bool wheelFixed);
        bool StopCar();
    }

    public class CarControlFactory
    {
        private struct EndPoint
        {
            public readonly string IP;
            public readonly int Port;

            public EndPoint(string ip, int port)
            {
                IP = ip;
                Port = port;
            }
        }

        private static readonly ConcurrentDictionary<EndPoint, ICarControl> CarControls = new ConcurrentDictionary<EndPoint, ICarControl>();

        public static ICarControl GetCarControl(string ip, int port)
        {
            var endPoint = new EndPoint(ip, port);
            ICarControl carControl = null;
            CarControls.TryGetValue(endPoint, out carControl);

            if (carControl == null)
            {
                carControl = new CarControl(ip, port);
                CarControls.TryAdd(endPoint, carControl);
            }

            return carControl;
        }
    }
}
