﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboticCar.PC.Common.InterfacesView
{
    public interface IDataView
    {
        void BindData<TData>(TData source);
        Action<Action> SyncInvoker { get; }
    }

    public interface IActionView<TCallback>
    {
        TCallback Callback { get; set; }
    }
}
