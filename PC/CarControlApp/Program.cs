﻿using System;
using System.Configuration;
using System.Windows.Forms;
using RoboticCar.PC.BL;
using RoboticCar.PC.CarControlUI;
using RoboticCar.PC.Common;
using RoboticCar.PC.CommunicationBriges.IncomingBridge;
using RoboticCar.PC.Controllers;

namespace RoboticCar.PC.CarControlApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Initialize();
            Application.Run(MainWindow);
            // TODO: add unhandled exceptions handler (AppDomain.UnhandledException) to prevent app crash
        }

        private static CarControlWindow MainWindow; // TODO: Add closing window event handler
                                                    // and stop clients and servers gracefully

        static void Initialize()
        {
            MainWindow = new CarControlWindow();

            // CarControlController
            var carControlView = MainWindow.GetCarControlView();
            var carControlController = new CarControlController(carControlView);

            // CarControlConectionController
            var carControlConectionView = carControlView.GetConnectionView;
            var connectionController = new CarControlConectionController(carControlConectionView);  // TODO: add default ip and port for connection - read from config file
                                                                                                    // Set it through properties/data object (NOT constructor)

            // Image processor
            int imageProcessorPort = int.Parse(ConfigurationManager.AppSettings[Constants.ImageProcessorDefaultPort]);  // TODO: add generic method to Utils class: T ReadConfigValue<T>(string key)
            var imageProcessor = new ImageProcessor();
            var imageProcessorServer = new ImageProcessorServer("Default image processor", imageProcessorPort, imageProcessor);
            imageProcessorServer.StartImageProcessorServer();
        }
    }
}
