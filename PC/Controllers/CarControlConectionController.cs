﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoboticCar.PC.Common.InterfacesCallback;
using RoboticCar.PC.Common.InterfacesView;
using RoboticCar.PC.CommunicationBriges.OutgoingBridge;

namespace RoboticCar.PC.Controllers
{
    public class CarControlConectionController : IConnectCallback
    {
        private IConnectView<IConnectCallback> _view;
        private CarControlController _carControlController;
        private ICarControl CarControl { get; set; }

        public CarControlConectionController(IConnectView<IConnectCallback> view)
        {
            view.Callback = this;
            _view = view;
        }

        #region IConnectCallback Members

        public void ConnectToServer(string ip, int port)
        {
            CarControl = CarControlFactory.GetCarControl(ip, port);
            if (CarControl.Initialize())
            {
                _carControlController = new CarControlController(_view.CarControlView)
                {
                    CarControl = CarControl
                };
            }
        }

        public void Disconnect()
        {
            _carControlController.CarControl = null;
            CarControl.Uninitialize();
        }

        #endregion
    }
}
