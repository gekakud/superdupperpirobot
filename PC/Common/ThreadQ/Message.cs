using System.Diagnostics;
using System.Reflection;
using System.Threading;
using RoboticCar.PC.Common.Logging;

namespace RoboticCar.PC.Common.ThreadQ
{
    public class ThreadMsg
    {
        protected bool _bSynchronized = false;
        private AutoResetEvent _event = null;
        protected bool _result = true;  //Result of operation. Dervied classes
                                        //can set this to true or false


        public bool Synchronized
        {
            get
            {
                return _bSynchronized;
            }
            set
            {
                _bSynchronized = value;
                if (_bSynchronized)
                    _event = new AutoResetEvent(false);
            }
        }
        
        public string ClassName
        {
            get
            {
                return this.GetType().Name;
            }
        }

        public void WaitForExecutionComplete()
        {
            if (_bSynchronized && _event != null)
            {
                Logger.Info("Synchronized call: Waiting for execution to complete");
                _event.WaitOne();
            }
        }

        public void SetExecutionComplete()
        {
            // if sync, set the event
            if (_bSynchronized && _event != null)
            {
                Logger.Info("Synchronized call: Execution Complete. Signalling the event");
                _event.Set();
            }
        }

        // VIRTUAl METHODS
        public void Execute()
        {
            _result = ProcessMessage();
        }
        public virtual bool ProcessMessage()
        {
            return true;
        }

        public virtual bool GetResult()
        {
            return _result;
        }
    }

    public class Message : ThreadMsg
    {
        public override bool ProcessMessage()
        {
            return true;
        }
    }
}
