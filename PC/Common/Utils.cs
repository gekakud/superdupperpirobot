﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoboticCar.PC.Common.Exceptions;
using RoboticCar.PC.Common.Logging;

namespace RoboticCar.PC.Common
{
    public class Utils
    {
        public static StartTaskException WaitForTaskStart(Task task, string taskName)
        {
            StartTaskException retEx = null;

            TaskStatus status;
            do
            {
                status = task.Status;
            } while (status < TaskStatus.Running);

            if (status == TaskStatus.Canceled)
                retEx = new StartTaskException("{0} task has been cancelled", null);
            else if(status == TaskStatus.Faulted)
                retEx = new StartTaskException("{0} task failed", task.Exception);
            
            return retEx;
        }
    }
}
