﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using RoboticCar.PC.Common.Logging;

namespace RoboticCar.PC.Common.Data
{
    public abstract class BindableProperty : IData
    {
        public Action<Action> SyncInvoker { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notify PropertyChanged event by using an expression.
        /// This method is preferred because it safely to rename properties.
        /// Usage:
        ///     NotifyPropertyChangedEvent( () => PropertyName );
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        protected void NotifyPropertyChangedEvent<T>(Expression<Func<T>> propertyExpression)
        {
            NotifyPropertyChangedEvent(ExtractPropertyName(propertyExpression));
        }

        /// <summary>
        /// Extract the property name and return it as string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        private string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            return (((MemberExpression)(propertyExpression.Body)).Member).Name;
        }

        /// <summary>
        /// Notify the UI that propertyName property have changed and data binding need be updated.
        /// </summary>
        /// <param name="propertyName">The name of changed property</param>
        private void NotifyPropertyChangedEvent(string propertyName)
        {
            try
            {
                //copy the PropertyChanged event handler to avoid the case when another thread will
                //set it to null between if statement and it's invocation
                PropertyChangedEventHandler propertyChanged = PropertyChanged;
                if (propertyChanged != null)
                {
                    if (SyncInvoker != null)
                        SyncInvoker(() => propertyChanged(this, new PropertyChangedEventArgs(propertyName)));
                    else
                        propertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while NotifyPropertyChangedEvent");
            }
        }

    }
}
