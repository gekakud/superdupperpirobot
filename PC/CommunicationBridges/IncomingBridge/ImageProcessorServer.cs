﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoboticCar.PC.Common;
using RoboticCar.PC.Common.Exceptions;
using RoboticCar.PC.Common.InterfacesBL;
using RoboticCar.PC.Common.Logging;
using Thrift.Server;
using Thrift.Transport;

namespace RoboticCar.PC.CommunicationBriges.IncomingBridge
{
    internal class ImageProcessorServerHandler : ImageProcessorThrift.Iface
    {
        private IImageProcessor Processor { get; set; }

        public ImageProcessorServerHandler(IImageProcessor processor)
        {
            Processor = processor;
        }

        #region Iface Members

        public bool ProcessImage(byte[] image)
        {
            if(Processor != null)
                Processor.ProcessImage(image);

            return true;
        }

        public bool RegisterCallback(string ip, int port)
        {
            throw new NotImplementedException();    // TODO: implement
        }

        #endregion
    }

    public class ImageProcessorServer
    {
        public string Name { get; private set; }
        private int _port;
        private IImageProcessor _imageProcessor;

        private TServer _server;
        private TServerTransport _transport;
        private Task _serverWorker;

        public ImageProcessorServer(string name, int port, IImageProcessor imageProcessor)
        {
            this.Name = name;
            this._port = port;
            this._imageProcessor = imageProcessor;
        }

        public void StartImageProcessorServer()
        {
            Logger.Info("Image Processor starting...");

            var handler = new ImageProcessorServerHandler(_imageProcessor);
            var processor = new ImageProcessorThrift.Processor(handler);

            _transport = new TServerSocket(_port);
            _server = new TThreadPoolServer(processor, _transport);

            _serverWorker = new Task(InternalStartImageProcessorServer);
            _serverWorker.Start();

            StartTaskException ex = Utils.WaitForTaskStart(_serverWorker, Name);
            if (ex != null)
                throw ex;

            Logger.Info("Image Processor started!");
        }

        private void InternalStartImageProcessorServer()
        {
            try
            {
                _server.Serve();
            }
            catch (Exception)
            {
                // TODO: how to handle task exceptions?
            }
            finally
            {
                FinalizeServer();
            }
        }

        public void StopImageProcessorServer()
        {
            FinalizeServer();
        }

        private void FinalizeServer()
        {
            try
            {
                _server.Stop();
                _server = null;
                _transport.Close();
                _transport = null;
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "ImageProcessorServer.FinalizeServer for [{0}] failed", Name);
            }
        }
    }
}
