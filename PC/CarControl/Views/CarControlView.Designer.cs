﻿namespace RoboticCar.PC.CarControlUI.Views
{
    partial class CarControlView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logText = new System.Windows.Forms.TextBox();
            this.backBtn = new System.Windows.Forms.Button();
            this.rightBtn = new System.Windows.Forms.Button();
            this.fwdBtn = new System.Windows.Forms.Button();
            this.leftBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.connectionView = new RoboticCar.PC.CarControlUI.Views.ConnectionView();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // logText
            // 
            this.logText.Location = new System.Drawing.Point(9, 134);
            this.logText.Multiline = true;
            this.logText.Name = "logText";
            this.logText.ReadOnly = true;
            this.logText.Size = new System.Drawing.Size(319, 318);
            this.logText.TabIndex = 19;
            // 
            // backBtn
            // 
            this.backBtn.Location = new System.Drawing.Point(405, 134);
            this.backBtn.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(84, 34);
            this.backBtn.TabIndex = 14;
            this.backBtn.Text = "BCWD";
            this.backBtn.UseVisualStyleBackColor = true;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // rightBtn
            // 
            this.rightBtn.Location = new System.Drawing.Point(457, 74);
            this.rightBtn.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.rightBtn.Name = "rightBtn";
            this.rightBtn.Size = new System.Drawing.Size(84, 36);
            this.rightBtn.TabIndex = 13;
            this.rightBtn.Text = "RIGHT";
            this.rightBtn.UseVisualStyleBackColor = true;
            this.rightBtn.Click += new System.EventHandler(this.rightBtn_Click);
            // 
            // fwdBtn
            // 
            this.fwdBtn.Location = new System.Drawing.Point(405, 26);
            this.fwdBtn.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.fwdBtn.Name = "fwdBtn";
            this.fwdBtn.Size = new System.Drawing.Size(84, 32);
            this.fwdBtn.TabIndex = 12;
            this.fwdBtn.Text = "FWD";
            this.fwdBtn.UseVisualStyleBackColor = true;
            this.fwdBtn.Click += new System.EventHandler(this.fwdBtn_Click);
            // 
            // leftBtn
            // 
            this.leftBtn.Location = new System.Drawing.Point(354, 74);
            this.leftBtn.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.leftBtn.Name = "leftBtn";
            this.leftBtn.Size = new System.Drawing.Size(84, 36);
            this.leftBtn.TabIndex = 11;
            this.leftBtn.Text = "LEFT";
            this.leftBtn.UseVisualStyleBackColor = true;
            this.leftBtn.Click += new System.EventHandler(this.leftBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.connectionView);
            this.groupBox1.Controls.Add(this.logText);
            this.groupBox1.Controls.Add(this.backBtn);
            this.groupBox1.Controls.Add(this.rightBtn);
            this.groupBox1.Controls.Add(this.fwdBtn);
            this.groupBox1.Controls.Add(this.leftBtn);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(551, 461);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // connectionView
            // 
            this.connectionView.Callback = null;
            this.connectionView.Location = new System.Drawing.Point(32, 18);
            this.connectionView.Name = "connectionView";
            this.connectionView.Size = new System.Drawing.Size(272, 110);
            this.connectionView.TabIndex = 20;
            // 
            // CarControlView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "CarControlView";
            this.Size = new System.Drawing.Size(551, 461);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox logText;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Button rightBtn;
        private System.Windows.Forms.Button fwdBtn;
        private System.Windows.Forms.Button leftBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private ConnectionView connectionView;
    }
}
