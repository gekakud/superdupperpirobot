﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using NLog;


namespace RoboticCar.PC.Common.Logging
{
    public sealed class Logger
    {
        #region Singleton

        private static readonly Logger LoggerInstance = new Logger();

        public static Logger Instance
        {
            get { return LoggerInstance; }
        }

        static Logger()
        {
        }

        private Logger()
        {
            this.Loggers = new ConcurrentDictionary<Type, ILogger>();
        }


        private ILogger CallerClassLogger
        {
            get
            {
                ILogger logger = null;
                Type t = new StackFrame(2).GetMethod().DeclaringType;
                if (t != null)
                {
                    if (!Loggers.TryGetValue(t, out logger))
                    {
                        logger = LogManager.GetLogger(t.Name);
                        Loggers.TryAdd(t, logger);
                    }
                }
                return logger;
            }
        }

        #endregion Singleton

        /// <summary>
        /// Loggers cache
        /// </summary>
        private ConcurrentDictionary<Type, ILogger> Loggers;

        public static void Info(string message)
        {
            Instance.CallerClassLogger.Info(message);
        }

        public static void Info(string message, params object[] args)
        {
            Instance.CallerClassLogger.Info(message, args);
        }

        public static void Trace(string message)
        {
            Instance.CallerClassLogger.Trace(message);
        }

        public static void Trace(string message, params object[] args)
        {
            Instance.CallerClassLogger.Trace(message, args);
        }

        public static void Warning(string message)
        {
            Instance.CallerClassLogger.Warn(message);
        }

        public static void Warning(string message, params object[] args)
        {
            Instance.CallerClassLogger.Warn(message, args);
        }

        public static void Error(string message)
        {
            Instance.CallerClassLogger.Error(message);
        }

        public static void Error(Exception ex, string message)
        {
            Instance.CallerClassLogger.Error(ex, message);
        }

        public static void Error(string message, params object[] args)
        {
            Instance.CallerClassLogger.Error(message, args);
        }

        public static void Error(Exception ex, string message, params object[] args)
        {
            Instance.CallerClassLogger.Error(ex, message, args);
        }
    }
}
