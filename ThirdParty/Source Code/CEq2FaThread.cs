using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using FAAdapter.Messages;
using FAAdapter.Logger;

namespace FAAdapter.Threads
{
    class CEq2FaThread 
    {
         private CEq2FaQueue m_eq2faQ;

        public CEq2FaThread(ref CEq2FaQueue q)
        {
            m_eq2faQ = q;
            Thread t = new Thread(new ThreadStart(this.Start));
            t.Start();
        }
        public void Start()
        {
            while (true)
            {
                try
                {
                    CMsg_Eq2Fa msg = m_eq2faQ.RemoveMessage();
                    FALogger.Instance.Information(TraceCategory.FABridgeMessage, "Going to process {0} message from FABrige", msg.className);
                    msg.Execute();
                    msg.SetExecutionComplete();
                }
                catch (Exception ex)
                {
                    FALogger.Instance.Information(TraceCategory.FABridgeMessage, "Unhandled problem while processing message {0}", ex.Message);
                    FALogger.Instance.Error(TraceCategory.FABridgeMessage, ex);
                }
            }
        }
    }
}
