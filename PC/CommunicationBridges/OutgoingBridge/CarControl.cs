﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoboticCar.PC.Common.Logging;
using Thrift.Protocol;
using Thrift.Transport;

namespace RoboticCar.PC.CommunicationBriges.OutgoingBridge
{
    public class CarControl : ICarControl
    {
        private CarControlThrift.Iface _client = null;
        private TTransport _transport = null;

        public string IP { get; private set; }
        public int Port { get; private set; }
        public bool IsInitialized { get; private set; }

        public CarControl(string ip, int port)
        {
            IP = ip;
            Port = port;
        }

        public bool Initialize()
        {
            try
            {
                if (!IsInitialized)
                {
                    _transport = new TSocket(IP, Port);
                    TProtocol protocol = new TBinaryProtocol(_transport);
                    _client = new CarControlThrift.Client(protocol);
                    _transport.Open();
                    IsInitialized = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Can't start CarControl client [{0}:{1}]. Check that server is started", IP, Port);
                _client = null;
            }

            return IsInitialized;
        }

        public void Uninitialize()
        {
            try
            {
                _transport.Close();
                IsInitialized = false;
                _client = null;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Can't close CarControl client connection");
            }
        }

        public bool StopCar()
        {
            bool res = false;
            if (_client != null)
                res = _client.StopCar();
            return res;
        }

        public bool GoForward(int speed)
        {
            bool res = false;
            if (_client != null)
                res = _client.GoForward(speed);
            return res;
        }

        public bool GoBackward(int speed)
        {
            bool res = false;
            if (_client != null)
                res = _client.GoBackward(speed);
            return res;
        }

        public bool TurnRight(int angle, bool wheelFixed)
        {
            bool res = false;
            if (_client != null)
                res = _client.TurnRight(angle, wheelFixed);
            return res;
        }

        public bool TurnLeft(int angle, bool wheelFixed)
        {
            bool res = false;
            if (_client != null)
                res = _client.TurnLeft(angle, wheelFixed);
            return res;
        }
    }
}
