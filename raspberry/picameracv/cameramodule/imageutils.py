from picameracv.common import common
from PIL import Image
import cv2
import os


def loadClassifier(objectType):
    print('running app in: ' + os.getcwd())
    classifier = []

    if objectType == common.object_types[0]:
        classifier.append(cv2.CascadeClassifier('classifiers\speed_limit_sign.xml'))
    elif objectType == common.object_types[1]:
        classifier.append(cv2.CascadeClassifier('classifiers\cars.xml'))
    elif objectType == common.object_types[2]:
        classifier.append(cv2.CascadeClassifier('classifiers\speed_limit_sign.xml'))
        classifier.append(cv2.CascadeClassifier('classifiers\cars.xml'))
    else:
        print('no such object type')
        exit(0)

    # TODO CascadeClassifier returns some value in any case,
    # TODO need more accurate check
    if classifier is None:
        print('no classifier')
        exit(0)

    return classifier


def detectObjects(data, classifier):
    gray = cv2.cvtColor(data, cv2.COLOR_BGR2GRAY)

    # Detect objects in the image, rectangles returned
    detections = classifier.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30),
                                             flags=cv2.CASCADE_SCALE_IMAGE)

    return detections


def drawDetectionsBorders(detections, colorIm):
    # Draw a rectangle over detection
    for detection in detections:
        x = detection[0]
        y = detection[1]
        w = detection[2]
        h = detection[3]

        cv2.rectangle(colorIm, (x, y), (x + w, y + h), color = common.detection_color, thickness = 2)

    return colorIm


def cropDetectionsAreas(detections, data):
    objectArr = []

    for rect in detections:
        x = rect[0]
        y = rect[1]
        w = rect[2]
        h = rect[3]

        # Crop from x, y, w, h
        # data[y: y + h, x: x + w]
        crop_img = data[y:y + h, x:x + w]
        objectArr.append(crop_img)

    return objectArr


def detectAndDrawObjectByType(data, classifier, objectType):
    # detect objects by given type
    detections = detectObjects(data, classifier)

    if len(detections) == 0:
        # cv2.destroyWindow("cropped detection")
        cv2.destroyWindow("cropped detection111")
    else:
        croppedObjects = cropDetectionsAreas(detections, data)

        combined_detections_img = []
        for object in croppedObjects:
            sizedObject = cv2.resize(object, (192, 192))
            combined_detections_img.append(sizedObject)

        v_combined = cv2.vconcat(combined_detections_img)
        cv2.imshow("cropped detection", v_combined)
        detectedObjects = drawDetectionsBorders(detections, data)
        return detectedObjects

    return data


def resizeImage(data, scaleFactor):
    resized = cv2.resize(data, None, fx=scaleFactor, fy=scaleFactor)
    return resized


def trackerImpl():
    tracker_types = ['BOOSTING', 'MIL', 'KCF', 'TLD', 'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']
    tracker_type = tracker_types[2]



# usage like this: createImageFile('PIC'+time.strftime("%d%m%Y"),dummyShit)
def createImageFile(fileName, data):
    # create a new image
    img = Image.new('RGB', (255, 255), "white")
    pixels = img.load()  # create the pixel map

    # put your data here
    for i in range(img.size[0]):  # for every pixel:
        for j in range(img.size[1]):
            pixels[i, j] = (i, j, 100)  # set the colour accordingly

    img.show()
    img.save(fileName + ".png", "PNG")
