﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoboticCar.PC.Common.InterfacesCallback;
using RoboticCar.PC.Common.InterfacesView;
using RoboticCar.PC.CommunicationBriges.OutgoingBridge;

namespace RoboticCar.PC.Controllers
{
    public class CarControlController : ICarControlCallback
    {
        public ICarControl CarControl { get; set; }
        private ICarControlView<ICarControlCallback> _view;

        public CarControlController(ICarControlView<ICarControlCallback> view)
        {
            view.Callback = this;
            _view = view;
        }

        #region ICarControlCallback Members

        public void GoForward(int speed)
        {
            if (CarControl != null)
                CarControl.GoForward(speed);
        }

        public void GoBack(int speed)
        {
            if (CarControl != null)
                CarControl.GoBackward(speed);
        }

        public void GoLeft(int angle, bool weelFixed)
        {
            if (CarControl != null)
                CarControl.TurnLeft(angle, weelFixed);
        }

        public void GoRight(int angle, bool weelFixed)
        {
            if (CarControl != null)
                CarControl.TurnRight(angle, weelFixed);
        }

        public void Stop()
        {
            if (CarControl != null)
                CarControl.StopCar();
        }

        #endregion
    }
}
