﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RoboticCar.PC.Common.InterfacesCallback;
using RoboticCar.PC.Common.InterfacesView;

namespace RoboticCar.PC.CarControlUI.Views
{

    public partial class CarControlView : UserControl, ICarControlView<ICarControlCallback>
    {
        public CarControlView()
        {
            InitializeComponent();
            connectionView.CarControlView = this;
        }


        #region IView<ICarControlCallback> Members

        public ICarControlCallback Callback { get; set; }

        #endregion

        public IConnectView<IConnectCallback> GetConnectionView
        {
            get { return connectionView; }
        }

        private void fwdBtn_Click(object sender, EventArgs e)
        {
            if(Callback != null)
                Callback.GoForward(5);
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            if (Callback != null)
                Callback.GoBack(5);
        }

        private void leftBtn_Click(object sender, EventArgs e)
        {
            if (Callback != null)
                Callback.GoLeft(2, false);
        }

        private void rightBtn_Click(object sender, EventArgs e)
        {
            if (Callback != null)
                Callback.GoRight(2, false);
        }

    }
}
