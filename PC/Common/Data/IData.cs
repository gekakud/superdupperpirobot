﻿using System;
using System.ComponentModel;

namespace RoboticCar.PC.Common.Data
{
    /// <summary>
    /// All data objects intended to be binded to UI
    ///    shall implement this interface
    /// </summary>
    public interface IData : INotifyPropertyChanged
    {
        /// <summary>
        /// The property shall return action that will
        /// synchronize data binding from any thread to
        /// UI thread.
        /// </summary>
        /// 
        /// <example>
        /// The typical code for returning an action:
        /// <code>
        /// public Action<Action> SynchronousInvoker
        /// {
        ///   get
        ///   {
        ///     return action =>
        ///       {
        ///           if (this.InvokeRequired)
        ///               this.BeginInvoke(action);
        ///           else
        ///               action();
        ///       };
        ///   }
        /// }
        /// </code>
        /// </example> 
        Action<Action> SyncInvoker { get; set; }
    }
}
