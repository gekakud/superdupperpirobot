using System.Collections;
using System.Collections.Concurrent;
using System.Threading;
using RoboticCar.PC.Common.Logging;

namespace RoboticCar.PC.Common.ThreadQ
{
    public class MsgQueue
    {
        private string Name;
        private ConcurrentQueue<Message> msgQ;
        public MsgQueue(string name)
        {
            Name = name;
            msgQ = new ConcurrentQueue<Message>();
        }
        
        public void AddMessage(Message msg)
        {
            Logger.Info("[{0}]: Waiting to Acquire lock inside AddMessage", Name);
            lock (msgQ)
            {
                Logger.Info("[{0}]: Acquired lock inside AddMessage", Name);
                msgQ.Enqueue(msg);
                Logger.Info("[{0}]: count after add: [{1}]", Name, msgQ.Count);
                Monitor.Pulse(msgQ);
            }
            Logger.Info("[{0}]: Released lock inside AddMessage", Name);
        }

        public object AddMessage(Message msg, bool bSync)
        {
            msg.Synchronized = bSync;
            AddMessage(msg);
            msg.WaitForExecutionComplete();
            return msg.GetResult();
        }
        
        public Message RemoveMessage()
        {
            Message msg;
            Logger.Info("[{0}]: Waiting to acquire lock inside RemoveMessage", Name);
            lock (msgQ)
            {
                while (msgQ.Count == 0)
                {
                    Logger.Info("[{0}]: Acquired lock inside RemoveMessage. Waiting for Items to be added", Name);
                    Monitor.Wait(msgQ);
                }
                Logger.Info("[{0}]: Going to dequeue message in RemoveMessage", Name);
                msgQ.TryDequeue(out msg);
                Logger.Info("[{0}]: count after remove: [{1}]", Name, msgQ.Count);
            }
            Logger.Info("[{0}]: Released lock inside RemoveMessage", Name);        
            return msg;
        }     
    }
}
