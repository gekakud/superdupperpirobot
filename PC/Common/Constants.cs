﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboticCar.PC.Common
{
    public class Constants
    {
        public const string CarControlDefaultPort = "CarControlPort";
        public const string ImageProcessorDefaultPort = "ImageProcessorPort";
    }
}
