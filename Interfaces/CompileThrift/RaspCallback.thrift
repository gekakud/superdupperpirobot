namespace csharp RoboticCar.PC.CommunicationBriges
namespace py RoboticCar.RPi.CommunicationBriges

service CarControlThrift
{
	bool StopCar(),
	bool GoForward(1:i32 speed),
	bool GoBackward(1:i32 speed),
	bool TurnRight(1:i32 angle, 2:bool wheelFixed),
	bool TurnLeft(1:i32 angle, 2:bool wheelFixed)
}