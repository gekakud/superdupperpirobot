from picameracv.cameramodule import camera
from picameracv.common import common


class MainLoopClass:
    @staticmethod
    def dummyProc():
        import time
        time.sleep(2)
        print ('Dummy proc worked')
        exit(0)

    def startMultipleProcs(self, procList):
        for i in range(len(procList)):
            currentProc = procList[i]
            if currentProc is 'dummy':
                self.dummyProc()
            print(str(currentProc))

    def __init__(self, mode=common.modes[0], cam_type=common.camera_types[1], object_type=common.object_types[0], procs=['dummy']):
        if (mode not in common.modes) | (object_type not in common.object_types):
            raise Exception("not found")

        if mode is common.modes[0]:
            camClass = camera.CameraClass(cam_type, object_type, delayTime=common.camera_sampling_delay[2])
            camClass.launchCameraDetection()
            exit(0)
        elif mode is common.modes[1]:
            self.startMultipleProcs(procList=procs)
