﻿using System.Windows.Forms;
using RoboticCar.PC.CarControlUI.Views;

namespace RoboticCar.PC.CarControlUI
{
    public partial class CarControlWindow : Form
    {
        public CarControlWindow()
        {
            InitializeComponent();
        }

        public CarControlView GetCarControlView()
        {
            return carControlView;
        }
    }
}