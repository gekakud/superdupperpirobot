modes = ['single_camera_process', 'multi']
object_types = ['sign', 'car', 'all']

camera_sampling_delay = [5, 10, 20, 50, 100]
camera_types = ['camera', 'web_camera', 'pi_camera']

# colors
detection_color = (0, 255, 240)