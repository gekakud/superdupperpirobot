﻿namespace RoboticCar.PC.Common.InterfacesBL
{
    public interface IImageProcessor
    {
        void ProcessImage(byte[] image);
    }
}
