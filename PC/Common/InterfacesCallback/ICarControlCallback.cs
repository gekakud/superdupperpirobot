﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboticCar.PC.Common.InterfacesCallback
{
    public interface ICarControlCallback
    {
        void GoForward(int speed);
        void GoBack(int speed);
        void GoLeft(int angle, bool weelFixed);
        void GoRight(int angle, bool weelFixed);
        void Stop();
    }
}
