﻿
using RoboticCar.PC.Common.InterfacesCallback;

namespace RoboticCar.PC.Common.InterfacesView
{
    public interface IConnectView<TCallback> : IActionView<TCallback>
    {
        ICarControlView<ICarControlCallback> CarControlView { get; set; }
    }
}