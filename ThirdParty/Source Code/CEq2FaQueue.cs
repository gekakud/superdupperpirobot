using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Threading;
using FAAdapter.Messages;
using FAAdapter.Logger;

using KLATencor.CommonApp.Logging;

namespace FAAdapter.Threads
{
    public class CEq2FaQueue
    {
        private Queue m_eq2faQ;
        public CEq2FaQueue()
        {
            m_eq2faQ = new Queue();
        }
        
        public void AddMessage(CMsg_Eq2Fa msg)
        {
            FALogger.Instance.Information(TraceCategory.Queue, "Waiting to Acquire lock inside AddMessage", TraceLevel.Elaborate);
            lock (m_eq2faQ)
            {
                FALogger.Instance.Information(TraceCategory.Queue, "Acquired lock inside AddMessage", TraceLevel.Elaborate);
                m_eq2faQ.Enqueue(msg);
                FALogger.Instance.Information(TraceCategory.Queue, string.Format("Eq2FaQ count after add: [{0}]", m_eq2faQ.Count), TraceLevel.Elaborate);
                Monitor.Pulse(m_eq2faQ);
            }
            FALogger.Instance.Information(TraceCategory.Queue, "Released lock inside AddMessage", TraceLevel.Elaborate);
        }

        public object AddMessage(CMsg_Eq2Fa msg, bool bSync)
        {
            msg.Synchronized = bSync;
            AddMessage(msg);
            msg.WaitForExecutionComplete();
            return msg.GetResult();
        }
        
        public CMsg_Eq2Fa RemoveMessage()
        {
            CMsg_Eq2Fa msg;
            FALogger.Instance.Information(TraceCategory.Queue, "Waiting to acquire lock inside RemoveMessage", TraceLevel.Elaborate);
            lock (m_eq2faQ)
            {
                while (m_eq2faQ.Count == 0)
                {
                    FALogger.Instance.Information(TraceCategory.Queue, "Acquired lock inside RemoveMessage. Waiting for Items to be added", TraceLevel.Elaborate);
                    Monitor.Wait(m_eq2faQ);
                }
                FALogger.Instance.Information(TraceCategory.Queue, "Going to dequeue message in RemoveMessage", TraceLevel.Elaborate);
                msg = (CMsg_Eq2Fa)m_eq2faQ.Dequeue();
                FALogger.Instance.Information(TraceCategory.Queue, string.Format("Eq2FaQ count after remove: [{0}]", m_eq2faQ.Count), TraceLevel.Elaborate);
            }
            FALogger.Instance.Information(TraceCategory.Queue, "Released lock inside RemoveMessage", TraceLevel.Elaborate);        
            return msg;
        }     
    }
}
