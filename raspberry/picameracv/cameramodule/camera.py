import cv2
import picameracv.cameramodule.imageutils as imgutils
from picameracv.common import common


class CameraClass:
    shared_var = 0

    def getNextFrame(self):
        if (self.sourceType is common.camera_types[0]) | (self.sourceType is common.camera_types[1]):
            try:
                ret, rawimage = self.stream.read()
            except Exception as e:
                print(e.message)
        elif self.sourceType is common.camera_types[2]:
            rawimage = self.stream.PiRGBArray(self.stream, size=(640, 480))
        return rawimage

    def connectCamera(self, sourceType):
        # open default camera
        if sourceType == common.camera_types[0]:
            stream = cv2.VideoCapture(0)
        elif sourceType == common.camera_types[1]:
            stream = cv2.VideoCapture(1)
        # elif sourceType == common.camera_types[2]:
        #     stream = PiCamera()
        else:
            print('No such source type')

        if stream.isOpened():
            print('stream opened')
        else:
            print('stream not opened')

        return stream

    def launchCameraDetection(self):

        import time
        time.sleep(2)
        while True:
            rawimage = self.getNextFrame()

            for classifier in self.classifiers:
                imagewithdetections = imgutils.detectAndDrawObjectByType(rawimage, classifier, self.objectType)
                # resize if needed and display colour image with detected objects
                # resized = imgutils.resizeImage(imagewithdetections, 0.8)
                cv2.imshow(self.objectType, imagewithdetections)
                # sleep, exit if presses Esc, terminate processes
                if cv2.waitKey(self.delayTime) == 27:
                    break

    def __init__(self, sourceType=common.camera_types[0], objectType='car', delayTime=50):
        self.sourceType = sourceType
        self.objectType = objectType

        if delayTime > 0:
            self.delayTime = delayTime
        else:
            raise Exception("Delay time is incorrect!!!")
            exit(0)

        self.classifiers = imgutils.loadClassifier(self.objectType)
        self.stream = self.connectCamera(self.sourceType)
