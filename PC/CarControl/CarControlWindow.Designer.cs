﻿namespace RoboticCar.PC.CarControlUI {
    partial class CarControlWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.carControlView = new RoboticCar.PC.CarControlUI.Views.CarControlView();
            this.SuspendLayout();
            // 
            // carControlView
            // 
            this.carControlView.Callback = null;
            this.carControlView.Location = new System.Drawing.Point(3, 3);
            this.carControlView.Name = "carControlView";
            this.carControlView.Size = new System.Drawing.Size(638, 536);
            this.carControlView.TabIndex = 0;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 530);
            this.Controls.Add(this.carControlView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "MainWindow";
            this.Padding = new System.Windows.Forms.Padding(22, 66, 22, 22);
            this.Text = "Server";
            this.ResumeLayout(false);

        }

        #endregion

        private RoboticCar.PC.CarControlUI.Views.CarControlView carControlView;

    }
}

