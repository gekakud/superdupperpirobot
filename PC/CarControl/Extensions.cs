﻿using System;
using System.ComponentModel;

namespace RoboticCar.PC.CarControlUI
{
    /// <summary>
    /// Contains simple, tested static methods for generic use.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// <para>Safe invoke method for getters.
        /// Check if InvokeRequired for a specified control and perform Invoke(action) if true.</para>
        /// <para>Usage example: string str = textBox.SafeInvoke(c => c.Text); </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="control"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static TResult SafeInvoke<T, TResult>(this T control, Func<T, TResult> action) where T : ISynchronizeInvoke
        {
            if (control.InvokeRequired)
            {
                IAsyncResult result = control.BeginInvoke(action, new object[] { control });
                object endResult = control.EndInvoke(result); return (TResult)endResult;
            }
            else
                return action(control);
        }
        /// <summary>
        /// <para>Safe invoke method for setters.
        /// Check if InvokeRequired for a specified control and perform Invoke(action) if true.</para>
        /// <para>Usage example: textBox.SafeInvoke(c => c.Text = text);</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="isi"></param>
        /// <param name="call"></param>
        public static void SafeInvoke<T>(this T isi, Action<T> call) where T : ISynchronizeInvoke
        {
            if (isi.InvokeRequired) isi.BeginInvoke(call, new object[] { isi });
            else
                call(isi);
        }
    }
}
