using System;
using System.Threading;
using RoboticCar.PC.Common.Logging;

namespace RoboticCar.PC.Common.ThreadQ
{
    class ThreadQueue
    {
        private MsgQueue _msgQ;
        private string Name;

        public ThreadQueue(string name)
        {
            Name = name;
            _msgQ = new MsgQueue(name);
            Thread t = new Thread(new ThreadStart(Start));
            t.Start();
        }

        public void Start()
        {
            while (true)
            {
                try
                {
                    Message msg = _msgQ.RemoveMessage();
                    Logger.Info("{[0}]: Going to process {1} message", Name, msg.ClassName);
                    msg.Execute();
                    msg.SetExecutionComplete();
                }
                catch (Exception ex)
                {
                    Logger.Info("[{0}]: Unhandled problem while processing message {1}", Name, ex.Message);
                    Logger.Error(ex.StackTrace);
                }
            }
        }
    }
}
