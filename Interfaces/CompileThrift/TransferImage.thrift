namespace csharp RoboticCar.PC.CommunicationBriges
namespace py RoboticCar.RPi.CommunicationBriges

service ImageProcessorThrift
{
	bool ProcessImage(1:binary image),
	bool RegisterCallback(1:string ip, 2:i32 port)
}